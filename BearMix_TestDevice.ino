/*
    This sketch establishes a TCP connection to a "quote of the day" service.
    It sends a "hello" message, and then prints received data.
*/

#define HWNAME "TestDevice"
#define HWVER "1.0"
#define HWTotalPort 4
#define FWVER "0.1029"// Send operate / send_coin event after sending coin done
#define BUILNOTE "2020-11-19_08:45"


//#include <ESP8266WiFi.h>

#define ESP32_PAYBOX_PCB_V1

#ifndef ESP32_PAYBOX_PCB_V1
#include <ESP8266WiFi.h>
#else
#include <WiFi.h>
#endif

// For Watchdog reset
#include <esp_task_wdt.h>
//200 seconds WDT
#define WDT_TIMEOUT 200
//--------------------------------------------------------------------------------------
// For NTP Client  https://randomnerdtutorials.com/esp32-ntp-client-date-time-arduino-ide/
//--------------------------------------------------------------------------------------
#include <NTPClient.h>
#include <WiFiUdp.h>

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

// Variables to save date and time
String NTPformattedDate;
String NTPdayStamp;
String NTPtimeStamp;
bool NTPSynced = false;

//--------------------------------------------------------------------------------------

// Related to HTTP/HTTPS OTA update
#include <Update.h>

//--------------------------------------------------------------------------------------

#include <PubSubClient.h>
#include <EEPROM.h>
#include "time.h"
//#include "GSheets.h"

#include "ArduinoJson.h"

#include <WebServer.h>
#include <ESPmDNS.h>
#include <Update.h>

WebServer server(80);

/*
   Login page
*/

const char* loginIndex =
  "<form name='loginForm'>"
  "<table width='20%' bgcolor='A09F9F' align='center'>"
  "<tr>"
  "<td colspan=2>"
  "<center><font size=4><b>ESP32 Login Page</b></font></center>"
  "<br>"
  "</td>"
  "<br>"
  "<br>"
  "</tr>"
  "<td>Username:</td>"
  "<td><input type='text' size=25 name='userid'><br></td>"
  "</tr>"
  "<br>"
  "<br>"
  "<tr>"
  "<td>Password:</td>"
  "<td><input type='Password' size=25 name='pwd'><br></td>"
  "<br>"
  "<br>"
  "</tr>"
  "<tr>"
  "<td><input type='submit' onclick='check(this.form)' value='Login'></td>"
  "</tr>"
  "</table>"
  "</form>"
  "<script>"
  "function check(form)"
  "{"
  "if(form.userid.value=='admin' && form.pwd.value=='admin')"
  "{"
  "window.open('/serverIndex')"
  "}"
  "else"
  "{"
  " alert('Error Password or Username')/*displays error message*/"
  "}"
  "}"
  "</script>";

/*
   Server Index Page
*/

const char* serverIndex =
  "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
  "<form method='POST' action='#' enctype='multipart/form-data' id='upload_form'>"
  "<input type='file' name='update'>"
  "<input type='submit' value='Update'>"
  "</form>"
  "<div id='prg'>progress: 0%</div>"
  "<script>"
  "$('form').submit(function(e){"
  "e.preventDefault();"
  "var form = $('#upload_form')[0];"
  "var data = new FormData(form);"
  " $.ajax({"
  "url: '/update',"
  "type: 'POST',"
  "data: data,"
  "contentType: false,"
  "processData:false,"
  "xhr: function() {"
  "var xhr = new window.XMLHttpRequest();"
  "xhr.upload.addEventListener('progress', function(evt) {"
  "if (evt.lengthComputable) {"
  "var per = evt.loaded / evt.total;"
  "$('#prg').html('progress: ' + Math.round(per*100) + '%');"
  "}"
  "}, false);"
  "return xhr;"
  "},"
  "success:function(d, s) {"
  "console.log('success!')"
  "},"
  "error: function (a, b, c) {"
  "}"
  "});"
  "});"
  "</script>";

/*
   setup function
*/


//const char* ntpServer = "pool.ntp.org";
//const long  gmtOffset_sec = 3600;
//const int   daylightOffset_sec = 3600;

const String month_name[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
const String day_name[7] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

int timezone = -(7 * 3600); //ตั้งค่า TimeZone ตามเวลาประเทศไทย
int dst = 0; //กำหนดค่า Date Swing Time
unsigned long task1_interval = 300;


#define EEPROM_SIZE 136  // 8 + 32 + 32 + 32 + 32
//-----------------------------
//    8: node id
//   32: SSID
//   32: PSW
//-----------------------------
#define NODE_ID_POS   0
#define SSID_POS      8
#define PSW_POS       40
#define OTA_POS       72


#define debugPrint 1

// --------------------------------------------------------------------------------
//  NODE config
// --------------------------------------------------------------------------------
//#define NODE_ID  "0000001"  // 7 digits
//#define MQ_DOWN_CHANNEL  "/MIXWASH/"
//#define MQ_UP_CHANNEL  "/MIXWASH/UP"
////#define DEF_TERMINAL_ID "TEST005"  // Terminal ID for Inventory
//#define DEF_TERMINAL_ID "P00001"  // Terminal ID for Inventory

// Node for BearMix MQTT
#define NODE_ID  "DEV0001"  // 7 digits
#define MQ_DOWN_CHANNEL  "device/"
#define MQ_UP_CHANNEL  "device/event"
//#define DEF_TERMINAL_ID "TEST005"  // Terminal ID for Inventory
#define DEF_TERMINAL_ID "DEV0001"  // Terminal ID for Inventory


#define QR_DISCOUNT_30THBUP

// --------------------------------------------------------------------------------
// ----- Pulse shaping ------------------------------------------------------------
// --------------------------------------------------------------------------------
const char PULSE_PER_COIN  = 10;
const unsigned char PULSE_OFF = 0;
const unsigned char PULSE_ON = 1;
// ---- Time unit in milliSec -------
//const unsigned long PULSE_ON_TIME  = 200;
//const unsigned long PULSE_OFF_TIME = 200;
//const unsigned long PULSE_GAP_TIME = 2000;   // Between coin
const unsigned long PULSE_ON_TIME  = 50;
const unsigned long PULSE_OFF_TIME = 100;
const unsigned long PULSE_GAP_TIME = 100;   // Between coin

unsigned long PulseOnCount = 0;
unsigned long PulseOffCount = 0;

//For operation
const unsigned long HEARTBEAT_TIME = 300000; // Send input status out every 5 minutes
const unsigned long INPUTCHG_MAX_RPT_TTIME = 5000; // Send input status out not less than 5 seconds

//For testing
//const unsigned long HEARTBEAT_TIME = 30000; // Send input status out every 5 minutes
//const unsigned long INPUTCHG_MAX_RPT_TTIME = 5000; // Send input status out not less than 5 seconds


// --------------------------------------------------------------------------------
//       NETWORK Config
// --------------------------------------------------------------------------------
// --- MQTT configuration ---------------------------
//#define MQTT_SERVER   "35.213.188.10"
//#define MQTT_SERVER   "35.213.188.100"
#define MQTT_PORT     1883
//#define MQTT_USERNAME "mixmix"
//#define MQTT_PASSWORD "c0vid1999"
//#define MQTT_NAME     "ESP32_1"

#define MQTT_SERVER   "188.166.235.19"
#define MQTT_USERNAME "mixi49_test001"
#define MQTT_PASSWORD "MixStep#49"

#define MQTT_NAME     NODE_ID

// --- WIFI  configuration ---------------------------
#ifndef STASSID
//#define STASSID "airbean2020"
//#define STAPSK  "Qx#2475@grEEn"
#define STASSID "TP-Link_0A64"
#define STAPSK  "mixiwash"
#endif

const char* ssid     = STASSID;
const char* password = STAPSK;


const String client_id = "610688108853-3d9df2apu3hj8hmftnn6gc9p4vu14rde.apps.googleusercontent.com";
const String client_secret = "vGlTlqqpgCSBQI7kMg6dKUgP";
const String sheet_id = "1reRW2YY8j0Iiv1weTvV1T8EHsL-DhFlnRHwdzwz7yto";
//GSheets gSheets;

// --- LOG Server configuration ---------------------------
//const char* serverName = "http://192.168.1.106:1880/update-sensor";
const char* serverName = "http://104.248.157.229/log";
const char* PayBoxServerName = "http://54.169.215.232:8000/paybox/log";
const char* AliveServerName = "http://54.169.215.232:8000/paybox/alive";

//char * logTemplate = "{ \n\
// \"machine\": {\n\
//    \"service_id\": \"INS0003\", \n\
//    \"site_id\": \t\"BW111\",\n\
//    \"machine_id\":  \"BW00100\"\n\
// },\n\
// \"payment\": {\n\
//    \"channel\": \"ThaiQR\",\n\
//    \"ref_id\": \"123\",\n\
//    \"price\": \"10.0\",\n\
//    \"detail\": {\n\
//        \"balance\": \"10:00\",\n\
//        \"point\": \"0\",\n\
//        \"discount\": \"0\"\n\
//    }\n\
// },\n\
// \"timestamp\": \"1594994574\"\r\n\
//}";

char * logTemplate = "{ \n\
 \"machine\": {\n\
    \"service_id\": \"INS00003\", \n\
    \"site_id\": \t\"BWM0001\",\n\
    \"machine_id\":  \"%s\"\n\
 },\n\
 \"payment\": {\n\
    \"channel\": \"none\",\n\
    \"ref_id\": \"none\",\n\
    \"price\": \"%s\",\n\
    \"detail\": %s\n\
 },\n\
 \"timestamp\": \"none\"\r\n\
}";

char * AliveLogTemplate = "{ \n\
    \"machine_id\":  \"%s\"\n\
}";

//--------------------------------------------------------------------------------//
// HTTP/HTTPS OTA Update
//--------------------------------------------------------------------------------//
bool HTTP_OTA_run = false;
int HTTP_OTA_UpdateCheck_State = 0;
// Variables to validate
// response from S3
long contentLength = 0;
bool isValidContentType = false;

// S3 Bucket Config
//String ota_host = "bucket-name.s3.ap-south-1.amazonaws.com"; // Host => bucket-name.s3.region.amazonaws.com
String ota_host;// = "165.22.105.250";
int ota_port = 80; // Non https. For HTTPS 443. As of today, HTTPS doesn't work.
//String ota_bin = "/sketch-name.ino.bin"; // bin file name with a slash in front.
String ota_bin;// = "/bearmix_ota/BearMix_TestDevice.FWver0.1013.bin";

char ch_str_host[128];
char ch_str_path[200];
int str_port;

int execOTA_retcode = 0;
char execOTA_retstr[100];
bool reboot_after_ota = false;

// --------------------------------------------------------------------------------
//       VARIENCE Declaration
// --------------------------------------------------------------------------------

const char* host = "djxmmx.net";
const uint16_t port = 17;

#ifdef ESP32_PAYBOX_PCB_V1
// Dx refers to ESP32 GPIO
const int D0 = 26;
const int D1 = 25;
const int D2 = 32;
const int D3 = 35;
const int D4 = 14;
const int D5 = 12;
const int D6 = 13;
const int D7 = 17;
#endif

int OUT_0 = D0;     // Pulse Coin to Washer
int OUT_1 = D1;     // Pulse Coin to Dryer
int OUT_2 = D2;     // Reserved
int OUT_3 = D3;     // Busy lamp
int IN_0  = D4;     // Input from Washer Coin
int IN_1  = D5;     // Inout from Washer Status
int IN_2  = D6;     // Input from Dryer Coin
int IN_3  = D7;     // Inout from Dryer Status
int INP[4] = {D4, D5, D6, D7};

char sBuff[512];

WiFiClient client;
PubSubClient mqtt(client);
WiFiClient ota_client;


int gotWifi = 0;
int firstMessage = 0;
//void pulse_ON();
//void pulse_OFF();

int coinCount = 0;
int pulseStatus;
int pulseCount = 0;

int ErrCount = 0;

//--------------------------------------------------------------------------------//
// Machine & Coin Monitoring
//--------------------------------------------------------------------------------//
char strLogPostData[512];

const int cBUSY_DENOISE_LIMIT = 10;
const int BUSY_SIGNAL_ACTIVE = 0;
const int BUSY_SIGNAL_IDLE = 1; // If no connect with machine
int busy_input3 = 0;
int busy_input2 = 0;
int busy_input1 = 0;
int busy_input0 = 0;
int Previous_BUSY_Status = BUSY_SIGNAL_IDLE;
int Current_BUSY_Status = BUSY_SIGNAL_IDLE;
int Force_BUSY_Status = 0;
int BUSY_denoise_count = 0;
int BUSY_CheckState = 0;
bool MachineBusy = false;

int coint1PulseIn = 0;
unsigned long StartCoin1InsertTime = 0;
unsigned long LastCoin1InsertTime = 0;
int coint1count = 0;

unsigned long SendCoinCountdown = 0;  // Countdown counter before sending coin to machine
unsigned long PaidCoin = 0;           // Total 10THB coin to send to machine
#define SENDCOIN_IDLE  ((coinCount == 0) && (SendCoinCountdown == 0))
#define cSENDCOIN_COUNTDOWN_VAL 15000;

// Interrupt Handler for Coin1 Input
void IRAM_ATTR detectsCoin1Input() {
  coint1PulseIn++;
  if (coint1PulseIn == 1)
  {
    StartCoin1InsertTime = millis();
  }
  else
  {
    LastCoin1InsertTime = millis();
  }
}
//--------------------------------------------------------------------------------//

void readRomString(int , int , char *);

//-----------------------------------------------------------------------------
//      Char & String
//-----------------------------------------------------------------------------
char TmpCharArray[50];

//-----------------------------------------------------------------------------
//      S E T    U P
//-----------------------------------------------------------------------------
char nodeIdStr[8];
char ssidStr[32];
char pswStr[32];
char otaStr[32];
char mqttName[16];
char terminalIdStr[16];

//-----------------------------------------------------------------------------
//      MQTT
//-----------------------------------------------------------------------------
char mqtt_cmdStr[32];
char mqtt_dataStr[200];
char mqtt_responseStr[200];

int  device_bootup = 1;
char DevicesJsonArray[200];

int  CmdDevicePort = 0;
bool MqttResponseFlag = false;
bool MqttCmdValidFlag = false;
unsigned long MqttResponseTimeOut = 0;

byte mqtt_connect_willQoS = 0;
const char* mqtt_connect_willTopic = "device/offline";
//const char* mqtt_connect_willMessage = "offline";
char mqtt_connect_willMessage[300];
boolean mqtt_connect_willRetain = false;

int Got10coinCount = 0;
bool SendCoinDoneFlag = false;

//-----------------------------------------------------------------------------

void setup() {

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(OUT_0, OUTPUT);
  pinMode(OUT_1, OUTPUT);
  pinMode(OUT_2, OUTPUT);
  pinMode(OUT_3, OUTPUT);

  pinMode(IN_0, INPUT_PULLUP); // Must pull up to detect falling interrupt
  pinMode(IN_1, INPUT_PULLUP); // Pull up if no connect with machine
  pinMode(IN_2, INPUT);
  pinMode(IN_3, INPUT);

  //Enable falling edge interrupt on Coin1 Input signal
  attachInterrupt(digitalPinToInterrupt(IN_0), detectsCoin1Input, FALLING);

  digitalWrite(OUT_0, PULSE_OFF);
  digitalWrite(OUT_1, PULSE_OFF);
  digitalWrite(OUT_2, 0);
  digitalWrite(OUT_3, 0);

  coint1PulseIn = 0;


  EEPROM.begin(EEPROM_SIZE);
  //int ledState = EEPROM.read(0);

  //Init SSID & PASSWD if necessary
  //  sprintf(terminalIdStr, NODE_ID);
  //  writeRomString(0, 8, s+1);
  //  sprintf(terminalIdStr, STASSID);
  //  writeRomString(8, 32, terminalIdStr);
  //  sprintf(terminalIdStr, STAPSK);
  //  writeRomString(40, 32, terminalIdStr);

  readRomString(NODE_ID_POS, 8, nodeIdStr);
  readRomString(SSID_POS,   32, ssidStr);
  readRomString(PSW_POS,    32,  pswStr);
  readRomString(OTA_POS,    32,  otaStr);

  sprintf(mqttName, "MW%s", nodeIdStr);
  sprintf(terminalIdStr, DEF_TERMINAL_ID);

  sprintf(mqtt_cmdStr, "Unknown");
  sprintf(mqtt_dataStr, "Unknown");
  sprintf(mqtt_responseStr, "Unknown");

  pulseOFF();
  PulseOffCount = 0;
  Serial.begin(115200);

  // Boot Up message
  Serial.println("///////////////////////////////////////////////");
  Serial.println("// Device booting up                         //");
  Serial.println("///////////////////////////////////////////////");
  Serial.println(" ");
  sprintf(sBuff, "     Hardware name : %s", HWNAME);
  Serial.println(sBuff);
  sprintf(sBuff, "           version : %s", HWVER);
  Serial.println(sBuff);
  sprintf(sBuff, "        Total Port : %d", HWTotalPort);
  Serial.println(sBuff);
  sprintf(sBuff, "  Firmware version : %s", FWVER);
  Serial.println(sBuff);
  sprintf(sBuff, "        Build Note : %s", BUILNOTE);
  Serial.println(sBuff);
  Serial.println(" ");
  Serial.println("///////////////////////////////////////////////");
  Serial.println(" ");

  //DevicesJsonString = "\"devices\": [";
  int Jidx;
  Jidx = sprintf(DevicesJsonArray, "\"devices\": [");
  for (int DevPort = 0; DevPort < HWTotalPort; DevPort++) {
    if ((DevPort + 1) == HWTotalPort) {
      sprintf(sBuff, "\n\"%s%s/%d\"]", MQ_DOWN_CHANNEL, nodeIdStr, DevPort + 1);
    } else {
      sprintf(sBuff, "\n\"%s%s/%d\",", MQ_DOWN_CHANNEL, nodeIdStr, DevPort + 1);
    }
    //DevicesJsonString += sBuff;
    Jidx += sprintf(DevicesJsonArray + Jidx, "%s", sBuff);
  }
  //DevicesJsonString += "\n]";
  Serial.println("######################################");
  sprintf(sBuff, ">> Devices List << [Char count = %d]", Jidx);
  Serial.println(sBuff);
  Serial.println(DevicesJsonArray);
  Serial.println("######################################");

  sprintf(mqtt_connect_willMessage, "{ \"id\" : \"device/%s\", %s }", nodeIdStr, DevicesJsonArray);
  Serial.print(">>> MQTT willMessage = ");
  Serial.println(mqtt_connect_willMessage);


  // Enable WDT reset
  esp_err_t WDT_ErrCode;

  WDT_ErrCode = esp_task_wdt_init(WDT_TIMEOUT, true);
  if ( WDT_ErrCode == ESP_OK ) {
    sprintf(sBuff, "Configuring WDT.., %dsec timeout", WDT_TIMEOUT);
    Serial.println(sBuff);

    WDT_ErrCode = esp_task_wdt_add(NULL);
    if ( WDT_ErrCode == ESP_OK ) {
      Serial.println("Successfully subscribed the task to the TWDT");
    } else {
      Serial.println("WDT add failed!");
    }

  } else {
    Serial.println("WDT Init failed!!");
  }

  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println("------ EEPROM Read ------");
  Serial.print("Node: ");
  Serial.println(nodeIdStr);
  Serial.print("MQ name: ");
  Serial.println(mqttName);
  Serial.print("SSID: ");
  Serial.println(ssidStr);
  Serial.print("PSW : ");
  Serial.println(pswStr);
  Serial.print("OTA : ");
  Serial.println(otaStr);
  Serial.println("-------------------------");

  device_bootup = 1;
  if(otaStr[0] != 'F'){
    Serial.println("<<<--- Init OTA area in EEPROM --->>>");
    sprintf(otaStr, "F0");
    writeRomString(OTA_POS, 32, otaStr);
  }
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssidStr);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  //WiFi.begin(ssid, password);
  WiFi.begin(ssidStr, pswStr);

  int wifiRetry = 40;

  while ((WiFi.status() != WL_CONNECTED) && (wifiRetry > 0)) {
    delay(500);
    Serial.print(".");
    WifiLED_toggle();
    wifiRetry--;
  }

  if (wifiRetry > 0) {

    gotWifi = 1;
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

    //    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
    //    printLocalTime();

    //configTime(timezone, dst, "pool.ntp.org"); //ดึงเวลาจาก Server
    configTime(timezone, dst, "pool.ntp.org", "time.nist.gov"); //ดึงเวลาจาก Server
    //configTime(timezone, dst, "time.nist.gov"); //ดึงเวลาจาก Server
    Serial.println("\nLoading time");
    while (!time(nullptr)) {
      Serial.print("*");
      delay(1000);
    }
    time_t now = time(nullptr);
    Serial.println(ctime(&now));
    printLocalTime();



    mqtt.setServer(MQTT_SERVER, MQTT_PORT);
    mqtt.setCallback(callback);

    Serial.println("Setup done");

  } else {
    //WifiLED_off();
    sprintf(sBuff, "Cannot connect to WIFI: %s", ssidStr);
    Serial.println(sBuff);
    Serial.println("Wait for SSID setting");
  }
  WifiLED_off();

  //sprintf(sBuff,logTemplate, nodeIdStr, "0", "{\"status\": \"power on\"}");
  //sprintf(sBuff,logTemplate, terminalIdStr, "0", "{\"status\": \"power on\"}"); // Use HardCode Terminal ID for Inventory instead of node_id/machine_id
  //sprintf(strLogPostData, "{\"status\": \"power on\",\"WIFI RSSI\": \"%d\"}", WiFi.RSSI());
  //sprintf(sBuff, logTemplate, terminalIdStr, "0", strLogPostData); // Use HardCode Terminal ID for Inventory instead of node_id/machine_id
  coint1PulseIn = 0;
  StartCoin1InsertTime = 0;;
  LastCoin1InsertTime = 0;;

  //Serial.print(sBuff);
  //sendLog(0, sBuff);

  //gSheets.begin(client_id,client_secret,sheet_id);

  Force_BUSY_Status = 0;
  Previous_BUSY_Status = BUSY_SIGNAL_IDLE;
  Current_BUSY_Status = BUSY_SIGNAL_IDLE;


  //**************************************************************//
  //**************************************************************//
  //  OTA Web Server
  //**************************************************************//
  //**************************************************************//
  /*use mdns for host name resolution*/
  if (!MDNS.begin(host)) { //http://esp32.local
    Serial.println("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }
  Serial.println("mDNS responder started");
  /*return index page which is stored in serverIndex */
  server.on("/", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", loginIndex);
  });
  server.on("/serverIndex", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", serverIndex);
  });
  /*handling uploading firmware file */
  server.on("/update", HTTP_POST, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
    ESP.restart();
  }, []() {
    HTTPUpload& upload = server.upload();
    if (upload.status == UPLOAD_FILE_START) {
      Serial.printf("Update: %s\n", upload.filename.c_str());
      if (!Update.begin(UPDATE_SIZE_UNKNOWN)) { //start with max available size
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_WRITE) {
      /* flashing firmware to ESP*/
      if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_END) {
      if (Update.end(true)) { //true to set the size to the current progress
        Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
      } else {
        Update.printError(Serial);
      }
    }
  });
  server.begin();

}

//-----------------------------------------------------------------------------
//      L O O P
//-----------------------------------------------------------------------------
void GetNTPDataTime(char Verbose) {
  // The formattedDate comes with the following format:
  // 2018-05-28T16:00:13Z
  // We need to extract date and time
  if (Verbose) Serial.print("FormattedDate : ");
  NTPformattedDate = timeClient.getFormattedDate();
  if (Verbose) Serial.print(NTPformattedDate);

  // Extract date
  int splitT = NTPformattedDate.indexOf("T");
  NTPdayStamp = NTPformattedDate.substring(0, splitT);
  if (Verbose) Serial.print(", DATE: ");
  if (Verbose) Serial.print(NTPdayStamp);
  // Extract time
  NTPtimeStamp = NTPformattedDate.substring(splitT + 1, NTPformattedDate.length() - 1);
  if (Verbose) Serial.print(", TIME: ");
  if (Verbose) Serial.println(NTPtimeStamp);
}

//-----------------------------------------------------------------------------
//      L O O P
//-----------------------------------------------------------------------------
int charCount = 0;
void loop() {

  server.handleClient();
  //delay(1);


  if (gotWifi == 1) {
    if (mqtt.connected() == false) {
      Serial.print("MQTT connection... ");
      WifiLED_on();
      delay(500);
      WifiLED_off();
      //if (mqtt.connect(mqttName, MQTT_USERNAME, MQTT_PASSWORD)) {
      if (mqtt.connect(mqttName, MQTT_USERNAME, MQTT_PASSWORD, mqtt_connect_willTopic, mqtt_connect_willQoS, mqtt_connect_willRetain, mqtt_connect_willMessage)) {
        Serial.println("connected");
        //WifiLED_on();
        WifiLED_off(); // WiFiLED off if both WiFi & MQTT are connected

        //DevicesJsonString = "\"devices\": [";
        //int Jidx;
        //Jidx = sprintf(DevicesJsonArray, "\"devices\": [");
        Serial.println("Start MQTT subscription: ");
        for (int DevPort = 0; DevPort < HWTotalPort; DevPort++) {
          sprintf(sBuff, "%s%s/%d", MQ_DOWN_CHANNEL, nodeIdStr, DevPort + 1);
          mqtt.subscribe(sBuff);
          Serial.println(sBuff);
          //if ((DevPort + 1) == HWTotalPort) {
          //  sprintf(sBuff, "\n\"%s%s/%d\"]", MQ_DOWN_CHANNEL, nodeIdStr, DevPort + 1);
          //} else {
          //  sprintf(sBuff, "\n\"%s%s/%d\",", MQ_DOWN_CHANNEL, nodeIdStr, DevPort + 1);
          //}
          //DevicesJsonString += sBuff;
          //Jidx += sprintf(DevicesJsonArray + Jidx, "%s", sBuff);
        }

      } else {
        Serial.println("failed");
        WifiLED_on();
        delay(500);
        WifiLED_off();
      }
    } else {

      if ( (device_bootup) && (NTPSynced) ) {

        if(otaStr[1] == '1'){
          sprintf(otaStr, "F0");  // Clear OTA updated flag
          writeRomString(OTA_POS, 32, otaStr);
          Serial.println("<<<--- Send MQTT OTA Update success to Server --->>>");
          sprintf(mqtt_cmdStr, "fw_updated");
          device_bootup = 0;
          MqttResponseFlag = true;
          MqttResponseTimeOut = timeClient.getEpochTime() + 2;
        } else {
          device_bootup = 0;
  
          sprintf(mqtt_cmdStr, "bootup");
          MqttResponseFlag = true;
          MqttResponseTimeOut = timeClient.getEpochTime() + 2;
        }

      }

      mqtt.loop();
    }
    // --- Execute subtask -------------------------
    unsigned long now = millis();
    //task1(now);
    pulseGen(now);
    inputCheck(now);
    MachineBusyCheck();
    Coin1InputCheck(now);
    SendCoinToMachineCheck();
    NTPTimeTask();
    MQTTResponseCheck();
    HTTP_OTA_UpdateCheck();

    //Test pulse gen//
    //if (coinCount == 0)
    //{
    //  coinCount = 10;
    //}
    //Test pulse gen//

  } else {
    WifiLED_blink(millis());
  }

  wifiCheck(millis());

  // --- Parse setting from serial port -------------------------
  if (Serial.available() > 0) { //ถ้าคอมพิวเตอร์ส่งข้อมูลมาใหจะทำใน if นี้
    addKey(Serial.read()); //นำค่าที่คอมพิวเตอร์ส่งมาเก็บในตัวแปร key
  }

  //##############################
  // If too many connection error  //
  if (ErrCount > 10)
  {
    Serial.println("Too many error, restart..");
    ESP.restart();
  }
  //##############################



}
//-----------------------------------------------------------------------------
//      Sub routines
//-----------------------------------------------------------------------------

int NTPState = 0;
void NTPTimeTask() {

  switch (NTPState) {
    case 0:
      if (gotWifi == 1) NTPState++;
      break;
    case 1:
      //**************************************************************//
      //**************************************************************//
      // Initialize a NTPClient to get time
      timeClient.begin();
      // Set offset time in seconds to adjust for your timezone, for example:
      // GMT +1 = 3600
      // GMT +8 = 28800
      // GMT -1 = -3600
      // GMT 0 = 0
      timeClient.setTimeOffset(7 * 3600);
      //**************************************************************//
      NTPState++;
      break;
    case 2:
      if (!timeClient.update()) {
        timeClient.forceUpdate();
      }
      else {
        // Current time report
        if (!NTPSynced) {
          if ( timeClient.getEpochTime() > 1604188800 ) { // If EpochTime > 1 Nov 2020
            NTPSynced = true;
            NTPState++;
          }
        }
      }
      break;
    case 3:
      if (gotWifi == 1) {
        if (!timeClient.update()) {
          timeClient.forceUpdate();
        }
      } else {
        timeClient.end();
        NTPSynced = false;
        NTPState = 0;
      }
      break;
    default:
      timeClient.end();
      NTPState = 0;
  }
}


void MQTTResponseCheck(void) {

  if (MqttResponseFlag)
  {
    if (timeClient.getEpochTime() >= MqttResponseTimeOut)
    {
      MqttResponseFlag = false;
      if (mqtt.connected() == true) {

        // Execute cmd
        if ( strcmp(mqtt_cmdStr, "fw_updated") == 0 ) {  // send OTA update done

          Serial.println("Send MQTT ota update done message");

          NTPformattedDate = timeClient.getFormattedDate();
          NTPformattedDate.toCharArray(TmpCharArray, sizeof(TmpCharArray));

          // Send MQTT event
          sprintf(sBuff, "{ \"id\" : \"device/%s\" , \"event\":\"fw_updated\", \n \"data\":{ \n\"DevTime\": \"%s\" , \n\"fwVer\": \"%s\" }  }",
                  nodeIdStr, TmpCharArray, FWVER );
          publishMqttTopic("device/event", sBuff);

          device_bootup = 1;

        } else if ( strcmp(mqtt_cmdStr, "bootup") == 0 ) {  // send boot up

          Serial.println("Send MQTT bootup message");

          NTPformattedDate = timeClient.getFormattedDate();
          NTPformattedDate.toCharArray(TmpCharArray, sizeof(TmpCharArray));

          // Send MQTT power on event
          sprintf(sBuff, "{ \"id\" : \"device/%s\" , \"event\":\"bootup\", \n \"data\":{ %s , \n\"DevTime\": \"%s\" , \n\"WIFI RSSI\": \"%d\" , \n\"SSID\": \"%s\" , \n\"PWD\": \"%s\" , \n\"hwName\": \"%s\" , \n\"hwVer\": \"%s\" , \n\"fwVer\": \"%s\" , \n\"build\": \"%s\" }  }",
                  nodeIdStr, DevicesJsonArray, TmpCharArray, WiFi.RSSI(), ssidStr, pswStr , HWNAME , HWVER , FWVER , BUILNOTE);
          publishMqttTopic("device/event", sBuff);

        } else if ( strcmp(mqtt_cmdStr, "alive") == 0 ) {  // send alive

          sprintf(sBuff, "{ \"id\" : \"device/%s\", %s }", nodeIdStr, DevicesJsonArray);
          publishMqttTopic("device/alive", sBuff);

        } else if ( strcmp(mqtt_cmdStr, "info") == 0 ) {  // Cmd = info

          Serial.println("Execute MQTT info command");

          NTPformattedDate = timeClient.getFormattedDate();
          NTPformattedDate.toCharArray(TmpCharArray, sizeof(TmpCharArray));

          sprintf(sBuff, "{ \"DevTime\": \"%s\" , \n\"WIFI RSSI\": \"%d\" , \n\"SSID\": \"%s\" , \n\"PWD\": \"%s\" , \n\"hwName\": \"%s\" , \n\"hwVer\": \"%s\" , \n\"fwVer\": \"%s\" , \n\"build\": \"%s\" }",
                  TmpCharArray, WiFi.RSSI(), ssidStr, pswStr , HWNAME , HWVER , FWVER , BUILNOTE);
          publishMqttResponseTopic(sBuff);

        } else if ( strcmp(mqtt_cmdStr, "restart") == 0 ) {  // Cmd = restart

          Serial.println("Execute MQTT restart command");

          sprintf(sBuff, "true");
          publishMqttResponseTopic(sBuff);

          Serial.println("ESP32 Restarting.....");
          delay(500);
          ESP.restart();

        } else if ( strcmp(mqtt_cmdStr, "send_coin") == 0 ) {    // Cmd = send_coin

          Serial.println("Execute MQTT send_coin command");

          sprintf(sBuff, "true");
          publishMqttResponseTopic(sBuff);

          SendCoinCountdown = cSENDCOIN_COUNTDOWN_VAL;
          PaidCoin = Got10coinCount;

        } else if ( strcmp(mqtt_cmdStr, "fw_ota") == 0 ) {    // Cmd = fw_ota

          Serial.println("Execute MQTT fw_ota command");

          sprintf(sBuff, "true");
          publishMqttResponseTopic(sBuff);

          HTTP_OTA_run = true;

        } else {
          Serial.println("Response for unknown MQTT command");

          sprintf(sBuff, "false");
          publishMqttResponseTopic(sBuff);

        }

      } else {
        Serial.println("MQTT Response: ** Failed!, MQTT disconnected ** ");
      }
    }
  } else {
    if(SendCoinDoneFlag){ // Request to send coin done event
      SendCoinDoneFlag = false;

      NTPformattedDate = timeClient.getFormattedDate();
      NTPformattedDate.toCharArray(TmpCharArray, sizeof(TmpCharArray));

      sprintf(sBuff, "{ \"id\" : \"device/%s/%d\" , \"event\":\"operate\", \n \"data\": {\n\"action\": \"send_coin\", \n\"10\": %d, \n\"timestamp\": \"%s\" } }", nodeIdStr, CmdDevicePort, PaidCoin, TmpCharArray);
      publishMqttTopic("device/event", sBuff);
    
    }
  }
}


unsigned long wifiCheckTime = 0;
void wifiCheck(unsigned long n) {
  if ((n - wifiCheckTime) > 5000) {
    sprintf(sBuff, "%ld: WIFI Check | ", n);
    Serial.print(sBuff);

    // Clear WDT every 5s
    esp_task_wdt_reset();

    // Current time report
    if (!NTPSynced) {
      Serial.println(" Waiting NTP Sync..");
    } else {
      GetNTPDataTime(0);
      Serial.print(" Time : ");
      Serial.println(NTPtimeStamp);
    }

    wifiCheckTime = n;
    if (WiFi.status() != WL_CONNECTED) {
      gotWifi = 0;
      Serial.println("Wifi disconnected");
      Serial.println("MQTT disconnected");
      mqtt.disconnect();
      ErrCount++;
    } else {
      ErrCount = 0;
      if (gotWifi == 0) {
        Serial.println("Wifi reconnected");
        Serial.println("MQTT reconnected");
        reInitMqtt();
      }
      gotWifi = 1;
      //reInitMqtt();
    }
  }
}

void reInitMqtt() {
  mqtt.setServer(MQTT_SERVER, MQTT_PORT);
  mqtt.setCallback(callback);
}



//int coinCount = 0;
//int pulseStatus;
//int pulseCount = 0;

void publishMq(char *s) {
  if (mqtt.connected() == true) {

    Serial.print("MQTT publish : ");
    Serial.println(s);

    mqtt.publish(MQ_UP_CHANNEL, s);
  }
  else {
    Serial.println("MQTT publish : ** disconnected ** ");
  }
}

void publishMqttResponseTopic(char *ds) {

  char PubMsg[512];

  if (mqtt.connected() == true) {

    Serial.print("MQTT publish response : topic = ");
    Serial.print(mqtt_responseStr);
    Serial.print(" , Device No = ");
    Serial.println(CmdDevicePort);

    NTPformattedDate = timeClient.getFormattedDate();
    NTPformattedDate.toCharArray(TmpCharArray, sizeof(TmpCharArray));

    sprintf(PubMsg, "{ \"id\" : \"device/%s/%d\" , \n\"cmd\":\"%s\", \n\"data\": \n%s\n }", nodeIdStr, CmdDevicePort, mqtt_cmdStr, ds);

    Serial.print("message = ");
    Serial.println(PubMsg);
    mqtt.publish(mqtt_responseStr, PubMsg);
  }
  else {
    Serial.println("MQTT publish response: ** Failed!, MQTT disconnected ** ");
  }
}

void publishMqttTopic(char *t, char *s) {

  if (mqtt.connected() == true) {

    Serial.print("MQTT publish : topic = ");
    Serial.println(t);
    Serial.print("message = ");
    Serial.println(s);

    mqtt.publish(t, s);
  }
  else {
    Serial.println("MQTT publish : ** disconnected ** ");
  }
}

void publishNodeInfo() {
  sprintf(sBuff,
          "{\"id\" : \"%s\", \"d\" :  \"rdy\" }",
          mqttName);
  publishMq(sBuff);
}


void publishInputStatus(char *i) {
  sprintf(sBuff,
          "{\"id\" : \"%s\", \n \
\"d\" :  \"i%d%d%d%d\" \
}",
          mqttName, i[0], i[1], i[2], i[3]);

  //Serial.println(sBuff);

  //publishMq(sBuff);
}




//int ready2run = 0;

// --------------------------------------------------------------------------------
//       Call back
// --------------------------------------------------------------------------------
void callback(char* topic, byte* payload, unsigned int length) {
  payload[length] = '\0';
  String topic_str = topic, payload_str = (char*)payload;
  Serial.println("[" + topic_str + "]: " + payload_str);

  //Checking topic
  char DeviceID_Port[30];
  for (int DevPort = 0; DevPort < HWTotalPort; DevPort++) {
    sprintf(DeviceID_Port, "%s%s/%d", MQ_DOWN_CHANNEL, nodeIdStr, DevPort + 1);
    if (strcmp(topic, DeviceID_Port) == 0) {
      sprintf(sBuff, "Found Topic for Device Port No = %d", DevPort + 1);
      Serial.println(sBuff);
      CmdDevicePort = DevPort + 1;
      parseJsonData(payload_str);
      return;
    }
  }
  Serial.println("*** Can't find Topic for specific device port ***");
}






//--------------- TASK ----------------------------------------
unsigned long task1Time = 0;
char task1Status = 0;
void task1(unsigned long n) {
  if ((n - task1Time) > task1_interval) {
    task1Time = n;
    //      if(task1Status == 0){
    //          task1Status = 1;
    //          digitalWrite(LED_BUILTIN, HIGH); // ไฟ LED 1 ติด
    //      }else{
    //          task1Status = 0;
    //          digitalWrite(LED_BUILTIN, LOW); // ไฟ LED 1 ดับ
    //      }
    //time_t now = time(nullptr);
    //Serial.print(p_tm->tm_year + 2443);
    printLocalTime();
    //      if(gotWifi == 1){
    //          addGoogleSheet();
    //      }
    //Serial.println(ctime(&now));
  }
}

//void addGoogleSheet(){
//    gSheets.addColumn("Temp",String(analogRead(A0)));
//    gSheets.addColumn("Millis",String(millis()));
//    if(gSheets.insertRow()){
//      Serial.println("---- inserted row ----");
//    }
//}

//--------------- Machine Busy Status Check ----------------------------------------
void SendCoinToMachineCheck() {

  //Start sending coind if Countdown counter = 1
  if (SendCoinCountdown == 1)
  {
    startPulseTrain(PaidCoin);

  }

  //Counting down
  if (SendCoinCountdown > 0)
  {
    SendCoinCountdown--;
  }

}


//--------------- Machine Busy Status Check ----------------------------------------
void MachineBusyCheck() {

  busy_input3 = busy_input2;
  busy_input2 = busy_input1;
  busy_input1 = busy_input0;
  busy_input0 = digitalRead(IN_1);

  // Check noise
  if ( (busy_input0 == busy_input1) &&
       (busy_input1 == busy_input2) &&
       (busy_input2 == busy_input3) )
  {
    // Ok, do nothing
  }
  else
  {
    // Noise, exit
    return;
  }

  Previous_BUSY_Status = Current_BUSY_Status;		// Previous = Current
  if (Force_BUSY_Status)
  {
    Current_BUSY_Status = BUSY_SIGNAL_ACTIVE;
  }
  else
  {
    Current_BUSY_Status = busy_input0;			// Current  = actual Input pin status
  }
  if (Previous_BUSY_Status != Current_BUSY_Status)	// If input signal change Then
  {
    if (Current_BUSY_Status == BUSY_SIGNAL_IDLE)
    {
      MachineBusy = false;
      if (SENDCOIN_IDLE)
      {
        Serial.println(">>>>Machine just STOP");
        sprintf(strLogPostData, "{\"status\": \"MachineSTOP\",\"BUSY\": \"%d\",\"FORCE\": \"%d\"}", Current_BUSY_Status, Force_BUSY_Status);
        sprintf(sBuff, logTemplate, terminalIdStr, "0", strLogPostData); // Use HardCode Terminal ID for Inventory instead of node_id/machine_id
        Serial.print(sBuff);
        sendLog(0, sBuff);
      }

    }
    else
    {
      MachineBusy = true;

      if (SENDCOIN_IDLE)
      {
        Serial.println(">>>>Machine just START");
        sprintf(strLogPostData, "{\"status\": \"MachineSTART\",\"coint1count\": \"%d\",\"PulseOnCount\": \"%d\",\"PulseOffCount\": \"%d\",\"BUSY\": \"%d\",\"FORCE\": \"%d\"}",
                coint1count, PulseOnCount, PulseOffCount, Current_BUSY_Status, Force_BUSY_Status);
        sprintf(sBuff, logTemplate, terminalIdStr, "0", strLogPostData); // Use HardCode Terminal ID for Inventory instead of node_id/machine_id
        Serial.print(sBuff);
        sendLog(0, sBuff);
      }

      coint1count = 0;
      PulseOnCount = 0;
      PulseOffCount = 0;
    }
  }
  //else if ((Current_BUSY_Status == 1) && (Previous_BUSY_Status == 1) && (MachineBusy == false))
  //{
  //  MachineBusy = true;
  //}
}

//--------------- Coin1InputCheck ----------------------------------------
void Coin1InputCheck(unsigned long n) {

  if ((coint1PulseIn != 0) && (LastCoin1InsertTime != 0)) // Condition to start measuring coin pulse period
  {
    if (LastCoin1InsertTime > n) // Protection for debuging (simulate insert coin by serial command)
    {
      if ((n - LastCoin1InsertTime) > 400) // If Last pulse time is more than ... Then
      {
        if (
          ((LastCoin1InsertTime - StartCoin1InsertTime) > 1000) && // Check pulse period
          ((LastCoin1InsertTime - StartCoin1InsertTime) < 2500) && //
          ((coint1PulseIn > 800) && (coint1PulseIn < 2000)) ) // Check pulse count
        {
          // Meet 10THB coin insert condition
          coint1count++;

          //Not sending pulse period
          if (SENDCOIN_IDLE)
          {
            Serial.println("Found 10THB Coin inserted");
            sprintf(strLogPostData, "{\"status\": \"GotCoin\",\"coint1PulseIn\": \"%d\",\"Coin1StartTime\": \"%d\",\"Coin1LastTime\": \"%d\",\"Coin1Count`\": \"%d\"}",
                    coint1PulseIn, StartCoin1InsertTime, LastCoin1InsertTime, coint1count);
            sprintf(sBuff, logTemplate, terminalIdStr, "0", strLogPostData); // Use HardCode Terminal ID for Inventory instead of node_id/machine_id
            Serial.print(sBuff);
            //sendLog(0, sBuff);
          }
          coint1PulseIn = 0;
          StartCoin1InsertTime = 0;;
          LastCoin1InsertTime = 0;;
        }
        else
        {
          // It's noise, ignore it and reset variable
          //Serial.println("Coin#1 IN noise..");
          coint1PulseIn = 0;
          StartCoin1InsertTime = 0;;
          LastCoin1InsertTime = 0;;
        }
      }
    }
  }
}


//--------------- InputCheck ----------------------------------------
//int old_P1 = 0, old_P2 = 0;
char old_p[4] = {0, 0, 0, 0};

unsigned long inputChangeTime = 0;
unsigned long inputCheckTime = 0;
unsigned long debounceTime = 0;
//unsigned long debounceTime1 = 0;
//unsigned long debounceTime2 = 0;
unsigned long debounceCoolDownTime = 50;
char inputCheckStatus = 0;
void inputCheck(unsigned long n) {
  int i;
  char p[4];

  if (
    ((n - inputCheckTime) > HEARTBEAT_TIME)) {
    inputCheckTime = n;
    for (i = 0; i < 4; i++) {
      p[i] = digitalRead(INP[i]);
    }
    sendInputStatus_HRB(p);

    //Not sending pulse period
    if (SENDCOIN_IDLE)
    {

      // Send Alive to PayBox Alive receiver
      Serial.println(">> Alive Interval <<");
      sprintf(mqtt_cmdStr, "alive");
      MqttResponseFlag = true;
      MqttResponseTimeOut = timeClient.getEpochTime() + 2;


    }

  }

  if ((n - debounceTime) > debounceCoolDownTime) {
    char hasChanged = 0;
    for (i = 0; i < 4; i++) {
      p[i] = digitalRead(INP[i]);
      if (old_p[i] != p[i]) {
        hasChanged = 1;
      }
      old_p[i] = p[i];
    }
    if (hasChanged == 1) {
      //        sendInputStatus(p);
      if ((millis() - inputChangeTime) > INPUTCHG_MAX_RPT_TTIME) {
        inputChangeTime = millis();
        sendInputStatus(p);
      }
    }
  }

}

//void sendInputStatus(int p1, int p2){
//    sprintf(sBuff,"INP:%d,%d",p1,p2);
//    Serial.println(sBuff);
//}
void sendInputStatus(char * p) {
  sprintf(sBuff, "INP:%d,%d,%d,%d", p[0], p[1], p[2], p[3]);
  Serial.println(sBuff);
  publishInputStatus(p);
}

void sendInputStatus_HRB(char * p) {
  sprintf(sBuff, "HRB INP:%d,%d,%d,%d", p[0], p[1], p[2], p[3]);
  Serial.println(sBuff);
  publishInputStatus(p);
}

void sendInputStatus_CHG(char * p) {
  sprintf(sBuff, "CHG INP:%d,%d,%d,%d", p[0], p[1], p[2], p[3]);
  Serial.println(sBuff);
  publishInputStatus(p);
}


//sprintf(sBuff,"INP:%d,%d",p1,p2);
//      report




//--------------- PULSE GENERATOR ----------------------------------------
int GEN_STATE = 0;
#define S_IDLE 0
#define S_ON   1
#define S_OFF  2
#define S_GAP  3

//const unsigned long PULSE_ON_TIME  = 20;
//const unsigned long PULSE_OFF_TIME = 100;


unsigned long pulseGenTime = 0;
char pulseGenStatus = 0;
void pulseGen(unsigned long n) {
char pulseGenStr[50];
  switch (GEN_STATE) {
    case (S_IDLE):   if (coinCount > 0) { // Start new coin
        Serial.println(">> New Coin");
        showTimeStamp();
        changeState(S_ON, n);
        //GEN_STATE = S_ON;
        //pulseGenTime = n;
        pulseON();
        pulseCount = PULSE_PER_COIN;
      }
      break;
    case (S_ON):     if ((n - pulseGenTime) > PULSE_ON_TIME) {
        //Serial.println(">> ON");
        changeState(S_OFF, n);
        //GEN_STATE = S_OFF;
        //pulseGenTime = n;
        pulseOFF();
      }
      break;
    case (S_OFF):    if ((n - pulseGenTime) > PULSE_OFF_TIME) {
        pulseCount--;
        //Serial.print("pulse count ");
        //Serial.println(pulseCount);

        if (pulseCount <= 0) {
          coinCount--;
          changeState(S_GAP, n);
          showTimeStamp();
          //GEN_STATE = S_GAP;
          //pulseGenTime = n;
          //Serial.println(">> GAP");

        } else {
          changeState(S_ON, n);
          //GEN_STATE = S_ON;
          //pulseGenTime = n;
          pulseON();
        }
      }
      break;
    case (S_GAP):    if ((n - pulseGenTime) > PULSE_GAP_TIME) {
        sprintf(pulseGenStr,">> EXIT GAP, coinCount = %d", coinCount);
        Serial.println(pulseGenStr);
        changeState(S_IDLE, n);
        //GEN_STATE = S_IDLE;
        //pulseGenTime = n;
        //                          if(coinCount <=0){
        //                              ready2run = 0;   // Finish command and wait for the next
        //                          }
        if (coinCount==0) { // Send coin done
          SendCoinDoneFlag = true;
        }
      }
      break;
    default:        GEN_STATE = S_IDLE;
      coinCount = 0;
      pulseCount = 0;

  }
}

void changeState(int s, unsigned long n) {
  //    Serial.println(s);
  GEN_STATE = s;
  pulseGenTime = n;
}

void pulseON() {
  PulseOnCount++;
  digitalWrite(LED_BUILTIN, PULSE_ON); // ไฟ LED 1 ติด
  digitalWrite(OUT_0, PULSE_ON);
  //    digitalWrite(OUT_1, PULSE_ON);
}

void pulseOFF() {
  PulseOffCount++;
  digitalWrite(LED_BUILTIN, PULSE_OFF); // ไฟ LED 1 ติด
  digitalWrite(OUT_0, PULSE_OFF);
  //    digitalWrite(OUT_1, PULSE_OFF);
}

void showTimeStamp() {
  unsigned long now = millis();
  sprintf(sBuff, "@%ld", now);
  Serial.println(sBuff);

}



//--------------- Pulse generator  subroutines ----------------------------------------
void startPulseTrain(unsigned long n) {
#ifdef debugPrint
  Serial.println(">> START TRAIN, coin = ");
  Serial.print(n);
#endif

  coinCount = n;

  unsigned long now = millis();
  pulseGenTime = now;   // Reset time on start pulse train
  //    pulseStatus = PULSE_ON;
  //    pulseOn();
  pulseCount = PULSE_PER_COIN;
}



void powerOn() {
  unsigned long now = millis() / 1000;
  Serial.println(now);
  Serial.println("ON");
  //digitalWrite(Relay1, LOW);
  cmdRelay(0, 0);
}

void powerOff() {
  unsigned long now = millis() / 1000;
  Serial.println(now);
  Serial.println("OFF");
  cmdRelay(0, 1);
}

void cmdRelay(char number, char flag) {
  char r;
  //  if(number == 0)r = Relay0;
  //  else r = Relay1;
  //  digitalWrite(r, flag);
}
//----------------------------------------------------------------------------------
//   String Parser
//----------------------------------------------------------------------------------
int strCount = 0;
char cmdStr[64];
void addKey(char c) {
  if ((c == 10) || (c == 13)) { // found CR or LF
    if (strCount > 0) {
      //sprintf (buffer, "FULL: %s",cmdStr);
      //Serial.println(buffer);
      cmdParse(cmdStr);
      strCount = 0;
      memset(cmdStr, '\0', sizeof(cmdStr));
      //clearStr(cmdStr);
    }
  } else {
    cmdStr[strCount] = c;
    if (strCount < 31) {
      strCount++;
    }
    cmdStr[strCount + 1] = 0;
    //Serial.println(cmdStr);
  }
}


void cmdParse( char* s) {
  int TestPort;
  
  Serial.println(s);

  switch (s[0]) {
    case ('n'):    Serial.print("Set NODE ID: ");
      Serial.println((s + 1));
      writeRomString(0, 8, s + 1);
      break;
    case ('s'):    Serial.print("Set SSID: ");
      Serial.println((s + 1));
      writeRomString(8, 32, s + 1);
      break;
    case ('p'):    Serial.print("Set PSW: ");
      Serial.println((s + 1));
      writeRomString(40, 32, s + 1);
      break;
    case ('r'):    Serial.println("ESP32 Restarting.....");
      ESP.restart();
      break;
    case ('i'):    Serial.println("Simulate Insert 10THB Coin");
      coint1PulseIn = 1000;
      StartCoin1InsertTime = millis();
      LastCoin1InsertTime = StartCoin1InsertTime + 1100;
      break;
    case ('m'):    Serial.print("Simulate Machine Start/Stop button");
      if (Force_BUSY_Status)
      {
        Serial.println(".. >>STOP<<");
        Force_BUSY_Status = 0;
      }
      else
      {
        Serial.println(".. >>START<<");
        Force_BUSY_Status = 1;
      }
      break;
    case ('c'):    Serial.println("Simulate Send Coin Signal to Machine");

      TestPort = s[1] - '0';
      TestPort = TestPort % 10;
      if (TestPort <= 0) TestPort = 1;
      CmdDevicePort = TestPort;

      PulseOnCount = 0;
      PulseOffCount = 0;
      coint1PulseIn = 0;
      if ((coint1count > 0) && (coint1count < 9)) {
        char GotCoinBuff[32];
        sprintf(GotCoinBuff, ">>> GotCoin %dTHB, Sending....", (coint1count * 10));
        Serial.println(GotCoinBuff);
        //startPulseTrain(coint1count);
        SendCoinCountdown = cSENDCOIN_COUNTDOWN_VAL;
        PaidCoin = coint1count;
      }
      break;
    case ('1'):    Serial.println(">>> Send Event <<<");
      if (mqtt.connected() == true) {
        sprintf(sBuff, "{ \"id\" : \"device/%s/1\" , \"event\":\"status\", \n \"data\":{ \"WIFI RSSI\": \"%d\" , \"SSID\": \"%s\" , \"PWD\": \"%s\" }  }", nodeIdStr, WiFi.RSSI(), ssidStr, pswStr );
        publishMqttTopic("response/DevTest", sBuff);
      }
      else {
        Serial.println("MQTT publish : ** disconnected ** ");
      }
      break;
    case ('2'):    Serial.println(">>> Send Alive <<<");

      sprintf(mqtt_cmdStr, "alive");
      MqttResponseFlag = true;
      MqttResponseTimeOut = timeClient.getEpochTime() + 2;

      break;
    case ('3'):    Serial.println(">>> Send Response <<<");

      if (mqtt.connected() == true) {

        sprintf(sBuff, "{ \"id\" : \"device/%s/%d\" , \"cmd\":\"%s\", \n \"data\": \"%s\", \n \"response\": \"%s\" }", nodeIdStr, CmdDevicePort, mqtt_cmdStr, mqtt_dataStr, mqtt_responseStr);
        char topicStr[100];
        publishMqttTopic(mqtt_responseStr, sBuff);
      }
      else {
        Serial.println("MQTT publish : ** disconnected ** ");
      }
      break;

    case ('4'):    Serial.println(">>> Send Example Payment log <<<");
      if (mqtt.connected() == true) {

        char ChannelStr[20];
        sprintf(ChannelStr, "PromptPay");
        char RefIdStr[20];
        //sprintf(RefIdStr, "3825148895279");
        sprintf(RefIdStr, "%d", timeClient.getEpochTime()); // Use Current EpochTime as Ref ID
        char PriceStr[50];
        //sprintf(PriceStr, "40.00");
        sprintf(PriceStr, "%.2f", (((float) (timeClient.getEpochTime() % 10000)) / 100)); // Dynamic test price from current EpochTime
        char DateTimeStr[50];
        //sprintf(DateTimeStr, "2020-11-04T04:10:08.000Z");
        NTPformattedDate = timeClient.getFormattedDate();
        NTPformattedDate.toCharArray(DateTimeStr, 50);
        char NoteStr[50];
        sprintf(NoteStr, "This is example payment message only");

        int TestPort;
        TestPort = s[1] - '0';
        TestPort = TestPort % 10;
        if (TestPort <= 0) TestPort = 1;
        sprintf(sBuff, "{ \"id\" : \"device/%s/%d\" , \"event\":\"payment\", \n \"data\": {\n\
          \"channel\": \"%s\",\n\
          \"ref_id\": \"%s\",\n\
          \"price\": \"%s\",\n\
          \"datetime\": \"%s\",\n\
          \"note\": \"%s\"} }", nodeIdStr, TestPort, ChannelStr, RefIdStr, PriceStr, DateTimeStr, NoteStr );

        publishMqttTopic("device/event", sBuff);
      }
      else {
        Serial.println("MQTT publish : ** disconnected ** ");
      }
      break;

    case ('5'):    Serial.println(">>> Send Device Info <<<");

      TestPort = s[1] - '0';
      TestPort = TestPort % 10;
      if (TestPort <= 0) TestPort = 1;
      CmdDevicePort = TestPort;
      sprintf(mqtt_cmdStr, "info");
      sprintf(mqtt_responseStr, "response/DevTest", nodeIdStr, CmdDevicePort);
      MqttResponseFlag = true;
      MqttResponseTimeOut = timeClient.getEpochTime() + 2;
      break;

    case ('6'):    Serial.println(">>> Send Device machine start <<<");

      if (mqtt.connected() == true) {

        int TestPort;
        TestPort = s[1] - '0';
        TestPort = TestPort % 10;
        if (TestPort <= 0) TestPort = 1;

        NTPformattedDate = timeClient.getFormattedDate();
        NTPformattedDate.toCharArray(TmpCharArray, sizeof(TmpCharArray));

        sprintf(sBuff, "{ \"id\" : \"device/%s/%d\" , \"event\":\"operate\", \n \"data\": {\n\"action\": \"start\", \n\"timestamp\": \"%s\" } }", nodeIdStr, TestPort, TmpCharArray);
        publishMqttTopic("device/event", sBuff);
      }
      else {
        Serial.println("MQTT publish : ** disconnected ** ");
      }
      break;

    case ('7'):    Serial.println(">>> Send Device machine stop <<<");

      if (mqtt.connected() == true) {

        int TestPort;
        TestPort = s[1] - '0';
        TestPort = TestPort % 10;
        if (TestPort <= 0) TestPort = 1;

        NTPformattedDate = timeClient.getFormattedDate();
        NTPformattedDate.toCharArray(TmpCharArray, sizeof(TmpCharArray));

        sprintf(sBuff, "{ \"id\" : \"device/%s/%d\" , \"event\":\"operate\", \n \"data\": {\n\"action\": \"stop\", \n\"timestamp\": \"%s\" } }", nodeIdStr, TestPort, TmpCharArray);
        publishMqttTopic("device/event", sBuff);
      }
      else {
        Serial.println("MQTT publish : ** disconnected ** ");
      }
      break;

    case ('8'):    Serial.println(">>> Send Device Error/Faulty <<<");

      if (mqtt.connected() == true) {

        int TestPort;
        TestPort = s[1] - '0';
        TestPort = TestPort % 10;
        if (TestPort <= 0) TestPort = 1;

        NTPformattedDate = timeClient.getFormattedDate();
        NTPformattedDate.toCharArray(TmpCharArray, sizeof(TmpCharArray));

        sprintf(sBuff, "{ \"id\" : \"device/%s/%d\" , \"event\":\"error\", \n \"data\": {\n\"err_code\": 404, \n\"timestamp\": \"%s\" } }", nodeIdStr, TestPort, TmpCharArray);
        publishMqttTopic("device/event", sBuff);
      }
      else {
        Serial.println("MQTT publish : ** disconnected ** ");
      }
      break;

    case ('0'):    Serial.println(">>> Start HTTP OTA update <<<");
      HTTP_OTA_run = true;
      break;

  }

}

//---------------------------------------------------------------
//  EEPROM Read & Write
//---------------------------------------------------------------

char eeBuff[32];
void writeRomString(int pos, int sizeLimit, char *s) {
  int i = 0;

  //Serial.println(pos);

  // --- Clear Memory -----
  for (i = 0; i < sizeLimit; i++)eeBuff[i] = 0;
  // --- Copy from s String ------
  for (i = 0; i < sizeLimit; i++) {
    if (*s > 0) {
      eeBuff[i] = *s;
      s++;
    } else {
      break;
    }
  }
  //Serial.println(eeBuff);

  //---  Write to EEPROM ----
  for (i = 0; i < sizeLimit; i++) {
    EEPROM.write(pos + i, eeBuff[i]);
  }
  EEPROM.commit();
}

void readRomString(int pos, int sizeLimit, char *s) {
  int i = 0;

  for (i = 0; i < sizeLimit; i++) {
    *s = EEPROM.read(pos + i);
    s++;
  }
  s[sizeLimit - 1] = 0;
}

//---------------------------------------------------------------
//  Wifi status
//---------------------------------------------------------------
int WifiLED_status = 0;

void WifiLED_on() {
  //    Serial.println("LED ON");
  //digitalWrite(OUT_3, 1);
  digitalWrite(LED_BUILTIN, HIGH); // ไฟ LED 1 ติด
}

void WifiLED_off() {
  //    Serial.println("LED OFF");
  //digitalWrite(OUT_3, 0);
  digitalWrite(LED_BUILTIN, LOW); // ไฟ LED 1 ดับ
}

void WifiLED_toggle() {
  if (WifiLED_status == 0) {
    WifiLED_on();
    WifiLED_status = 1;
  } else {
    WifiLED_off();
    WifiLED_status = 0;
  }
}

unsigned long WifiLED_Time = 0;
void WifiLED_blink(unsigned long n) {
  if ((n - WifiLED_Time) > 100) {
    WifiLED_Time = n;
    if (WifiLED_status == 0) {
      WifiLED_status = 1;
      //digitalWrite(OUT_3, HIGH); // ไฟ LED 1 ติด
      digitalWrite(LED_BUILTIN, HIGH); // ไฟ LED 1 ติด
    } else {
      WifiLED_status = 0;
      //digitalWrite(OUT_3, LOW); // ไฟ LED 1 ดับ
      digitalWrite(LED_BUILTIN, LOW); // ไฟ LED 1 ติด
    }
  }
}

//---------------------------------------------------------------
//  TIME sync
//---------------------------------------------------------------
time_t startUpTime = 0;
int startUpFlag = 0;
double wakeUpTime;

void printLocalTime() {
  time_t now = time(nullptr);
  time_t n2;

  time(&n2);
  struct tm* p_tm = localtime(&now);

  if (p_tm->tm_year > 110) {
    if (startUpFlag == 0) {
      Serial.println("Time ready");
      WifiLED_on();
      task1_interval = 60000;
      time(&startUpTime);
      Serial.println(ctime(&startUpTime));
      startUpFlag = 1;
    }
    sprintf(sBuff, "%02d:%02d:%02d" , p_tm->tm_hour, p_tm->tm_min, p_tm->tm_sec);
    Serial.print(sBuff);
    Serial.print(" ( ");
    Serial.print(day_name[(p_tm->tm_wday)]);
    Serial.print(" ");
    Serial.print(p_tm->tm_mday);
    Serial.print(" / ");
    Serial.print(month_name[(p_tm->tm_mon)]);
    Serial.print(" / ");
    Serial.print(p_tm->tm_year + 2443);
    Serial.println(")");
    wakeUpTime = difftime(n2 , startUpTime);
    sprintf(sBuff, "Diff %.2f sec", wakeUpTime);
    Serial.println(sBuff);
  } else {
    WifiLED_toggle();
    Serial.print("@");
  }
  //   char * testStr = "20/5/2020, 14:02:30";
  //   int yy,MM,dd, hh,mm,ss;
  //   sscanf( testStr, "%d/%d/%d, %d:%d:%d", &dd, &MM, &yy, &hh, &mm, &ss);
  //   sprintf(sBuff, "%d:%d:%d (%d-%d-%d)", hh, mm, ss, dd,MM,yy);
  //   Serial.println(sBuff);
}

//---------------------------------------------------------------
//  Parse json data from MQTT subscribe message
//---------------------------------------------------------------
void parseJsonData(String s) {

  StaticJsonDocument<512> Jdoc;
  //DeserializationError json_error = deserializeJson(Jdoc, s,DeserializationOption::NestingLimit(5));
  DeserializationError json_error = deserializeJson(Jdoc, s);

  if (json_error) {   //Check for errors in parsing
    Serial.println("Jason Parsing failed");
    return;
  }

  JsonVariant Jobj_cmd = Jdoc.getMember("cmd");
  if (Jobj_cmd.isNull()) {   //Check for errors in parsing
    Serial.println("Jason Parsing : \"cmd\" not found");
    return;
  }

  if (MqttResponseFlag || HTTP_OTA_run ) {
    Serial.print(">>>> Ignored MQTT received message: ");
    if (MqttResponseFlag) {
      Serial.print("[previous sending is not finish yet] ");
    }
    if (HTTP_OTA_run) {
      Serial.print("[during OTA update] ");
    }
    Serial.println("<<<<");
    return;
  }
  
  sprintf(mqtt_cmdStr, Jobj_cmd.as<const char*>());
  Serial.print("Jason parsing\ncmd: ");
  Serial.println(mqtt_cmdStr);

  JsonVariant Jobj_response = Jdoc.getMember("response");
  if (Jobj_response.isNull()) {   //Check for errors in parsing

    if ( strcmp(mqtt_cmdStr, "payment") == 0 ) {
      Serial.println("Jason Parsing \"response\" not need for payment");
    } else {    
      Serial.println("Jason Parsing \"response\" not found");
      return;
    }
  } else {
    sprintf(mqtt_responseStr, Jobj_response.as<const char*>());
    Serial.print("response: ");
    Serial.println(mqtt_responseStr);
  }

  // Execute cmd
  if ( strcmp(mqtt_cmdStr, "info") == 0 ) {  // Cmd = info

    Serial.println("Execute Cmd : \"info\", (no cmd data)");
    MqttCmdValidFlag = true;
    MqttResponseFlag = true;
    MqttResponseTimeOut = timeClient.getEpochTime() + 2;

  } else if ( strcmp(mqtt_cmdStr, "restart") == 0 ) {  // Cmd = restart

    Serial.println("Execute Cmd : \"restart\", (no cmd data)");
    MqttResponseFlag = true;
    MqttResponseTimeOut = timeClient.getEpochTime() + 2;

  } else if ( strcmp(mqtt_cmdStr, "send_coin") == 0 ) {    // Cmd = send_coin

    Serial.println("Execute Cmd : \"send_coin\", (check data)");

    sprintf(sBuff, ">> Number of nesting in Jason = %d", Jdoc.nesting());
    Serial.println(sBuff);
    if (Jdoc.nesting() > 1) {
      Got10coinCount = Jdoc["data"]["10"];
      sprintf(sBuff, ">> Device Port No: %d , Coin 10 = %d", CmdDevicePort, Got10coinCount);
      Serial.println(sBuff);

      MqttResponseFlag = true;
      MqttResponseTimeOut = timeClient.getEpochTime() + 2;

    } else {
      Serial.println("Can't find data of this command");
    }
  } else if ( strcmp(mqtt_cmdStr, "fw_ota") == 0 ) {    // Cmd = fw_ota

    Serial.println("Execute Cmd : \"fw_ota\", (check data)");

    sprintf(sBuff, ">> Number of nesting in Jason = %d", Jdoc.nesting());
    Serial.println(sBuff);
    if (Jdoc.nesting() > 1) {
      const char* str_host = Jdoc["data"]["host"];
      str_port = Jdoc["data"]["port"];
      const char* str_path = Jdoc["data"]["path"];

      strcpy( ch_str_host, str_host);
      strcpy( ch_str_path, str_path);

      ota_host = "";
      ota_host += ch_str_host;

      ota_bin = "";
      ota_bin += ch_str_path;

      sprintf(sBuff, ">> Device Port No: %d \n>>Host = \"", CmdDevicePort);
      Serial.print(sBuff);
      Serial.print(str_host);
      sprintf(sBuff, "\"\n>>Port = %d\n>>Path = \"", str_port);
      Serial.print(sBuff);
      Serial.print(str_path);
      Serial.println("\"");
      
      MqttResponseFlag = true;
      MqttResponseTimeOut = timeClient.getEpochTime() + 2;

    } else {
      Serial.println("Can't find data of this command");
    }

  } else if ( strcmp(mqtt_cmdStr, "payment") == 0 ) {    // Cmd = payment

    Serial.println("Execute Cmd : \"payment\", (check data)");

    sprintf(sBuff, ">> Number of nesting in Jason = %d", Jdoc.nesting());
    Serial.println(sBuff);
    if (Jdoc.nesting() > 1) {
      
      Got10coinCount = Jdoc["data"]["coinCount"];

      sprintf(sBuff, ">>> Got coinCount = %d", Got10coinCount);
      Serial.println(sBuff);
      Got10coinCount = Got10coinCount/10;

      if( ( Got10coinCount > 0 ) && ( Got10coinCount < 6 ) ) {
        sprintf(sBuff, ">>> 10 coin = %d, Send to machine...", Got10coinCount);
        Serial.println(sBuff);
        SendCoinCountdown = cSENDCOIN_COUNTDOWN_VAL;
        PaidCoin = Got10coinCount;
      } else {
        sprintf(sBuff, ">>> Too many 10 coin (%d)", Got10coinCount);
        Serial.println(sBuff);
      }

    } else {
      Serial.println("Can't find data of this command");
    }
  } else {
    Serial.println("Unknown Cmd, ignore");
    MqttResponseFlag = true;
    MqttResponseTimeOut = timeClient.getEpochTime() + 2;
  }

  Serial.println();


}

//---------------------------------------------------------------
//  Parse message from master server
//---------------------------------------------------------------
void parseMasterCommand(String s) {

  switch (s[0]) {
    case ('i'):  firstMessage = 1;  // Initialize
      Serial.println("init MQTT message Queue");
      break;
    case ('c'):  if (firstMessage == 1) {
        unsigned char c = s[1];   // Get coin number
        //s[2] = 0;
        c -= '0';
        //sprintf(sBuff,logTemplate, nodeIdStr, s, "{\"status\": \"got coin\"}");
        //                      char tempBuff[8];
        //                      sprintf(tempBuff, "%d0.00", c);
        //                      sprintf(sBuff,logTemplate, nodeIdStr, tempBuff, "{\"status\": \"got coin\"}");
        //
        //
        //                      //sprintf(sBuff,logTemplate, nodeIdStr, "10.00", "{\"status\": \"got coin\"}");
        //                      Serial.print(sBuff);
        //                      sendLog(sBuff);
        sendGotCoinlog(s);

        /*
          if((c>0)&&(c<9)){
            #ifndef QR_DISCOUNT_30THBUP
              startPulseTrain(c);
            #else
              char GotCoinBuff[32];
              if(c >= 3)
              {
                sprintf(GotCoinBuff, ">>> C %d >= 30THB, +10THB free", c);
                Serial
                .println(GotCoinBuff);
                startPulseTrain(c+1);
              }
              else
              {
                sprintf(GotCoinBuff, ">>> C %d < 30THB, No discount", c);
                Serial.println(GotCoinBuff);
                startPulseTrain(c);
              }
            #endif
          }
        */

        if ((c > 0) && (c < 9)) {
#ifndef QR_DISCOUNT_30THBUP
          SendCoinCountdown = cSENDCOIN_COUNTDOWN_VAL;
          PaidCoin = c;
#else
          char GotCoinBuff[32];
          if (c >= 3)
          {
            sprintf(GotCoinBuff, ">>> C %d >= 30THB, +10THB free", c);
            Serial.println(GotCoinBuff);
            SendCoinCountdown = cSENDCOIN_COUNTDOWN_VAL;
            PaidCoin = c + 1;
          }
          else
          {
            sprintf(GotCoinBuff, ">>> C %d < 30THB, No discount", c);
            Serial.println(GotCoinBuff);
            SendCoinCountdown = cSENDCOIN_COUNTDOWN_VAL;
            PaidCoin = c;
          }
#endif
        }


      }
      break;
  }
  //   if(firstMessage == 1){
  //        firstMessage = 0; //Ignore the first message
  //   }else{
  //        unsigned char c = payload_str[0];   // Get coin number
  //        c -= '0';
  //        if((c>0)&&(c<9)){
  //            startPulseTrain(c);
  //        }
  //    }
}

//  sendLog : HTTP Post Jason to destination server
//  int dest : code for destination server
//  char* s  : Server URL string
void sendLog(int dest, char* s) {
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println(">> Sending log");

    //HTTPClient http;

    // Your Domain name with URL path or IP address with path
    switch (dest) {
      case (1):
        //http.begin(AliveServerName);  // dest = 1 : For Alive
        publishMqttTopic("device/alive", s);
        break;
      case (2):
        //http.begin(PayBoxServerName); // dest = 2 : For vending log server
        publishMqttTopic("device/event", s);
        break;
      default:
        //http.begin(serverName);     // dest = default : For power on
        publishMqttTopic("device/event", s);
    }

    /*
          // Specify content-type header
          //http.addHeader("Content-Type", "application/x-www-form-urlencoded");
          // Data to send with HTTP POST
          //String httpRequestData = "api_key=tPmAT5Ab3j7F9&sensor=BME280&value1=24.25&value2=49.54&value3=1005.14";
          // Send HTTP POST request
          //int httpResponseCode = http.POST(httpRequestData);

          // If you need an HTTP request with a content type: application/json, use the following:
          http.addHeader("Content-Type", "application/json");
          //int httpResponseCode = http.POST("{\"api_key\":\"tPmAT5Ab3j7F9\",\"sensor\":\"BME280\",\"value1\":\"24.25\",\"value2\":\"49.54\",\"value3\":\"1005.14\"}");
          int httpResponseCode = http.POST(s);

          // If you need an HTTP request with a content type: text/plain
          //http.addHeader("Content-Type", "text/plain");
          //int httpResponseCode = http.POST("Hello, World!");

          Serial.print("HTTP Response code: ");
          Serial.println(httpResponseCode);

          // Free resources
          http.end();
    */
    /*
      if(httpResponseCode == ??)
      {
      ErrCount = 0;
      }
      else
      {
      ErrCount++;
      }
    */

  }
}

void sendGotCoinlog(String s) {
  // ---- copy string -----
  char a[s. length() + 1];
  int i;
  for (i = 0; i < s. length(); i++) {
    a[i] = s[i];
  }
  a[i] = 0;



  char temp_ref[5][50];

  char delim[] = ",";
  char *ptr = strtok(a, delim);

  int j = 0;
  while ((ptr != NULL) && (j < 5)) {
    strcpy(temp_ref[j], ptr);
    j++;
    Serial.println(ptr);
    ptr = strtok(NULL, delim);
  }

  for (j = 0; j < 5; j++) {
    Serial.print(j);
    Serial.println(temp_ref[j]);
  }



  char * logTemplate2 = "{ \n\
 \"machine\": {\n\
    \"service_id\": \"INS00003\", \n\
    \"site_id\": \t\"BWM0001\",\n\
    \"machine_id\":  \"%s\"\n\
 },\n\
 \"payment\": {\n\
    \"channel\": \"%s\",\n\
    \"ref_id\": \"%s\",\n\
    \"price\": \"%s\",\n\
    \"detail\": %s\n\
 },\n\
 \"timestamp\": \"%s\"\n\
}";

  int time_len = strlen(temp_ref[4]);
  temp_ref[4][time_len] = 0; // add 0 after last character

  int c = temp_ref[0][1] - '0';
  char tempBuff[8];
  sprintf(tempBuff, "%d0.00", c);

  char ChannelStr[10];
  sprintf(ChannelStr, "PromptPay");

  char option_data[128];
  sprintf(option_data, "{\"status\": \"got coin\", \"time\": \"%s\"}", temp_ref[4]);

  sprintf(sBuff, logTemplate2,
          //            nodeIdStr,
          terminalIdStr,    // Use HardCode Terminal ID for Inventory instead of node_id/machine_id

          //            temp_ref[1],      // Bank number
          ChannelStr,         // Channel = PromptPay

          //temp_ref[3],                  // transcation ID
          temp_ref[1],        // ref_id = bank account no

          tempBuff,                     // Price
          //            "{\"status\": \"got coin\"}"  // Option data
          //            );
          //"{\"status\": \"got coin\"}", // Option data
          option_data,    // Option data

          temp_ref[4]);   // Time stamp

  Serial.print(sBuff);
  sendLog(2, sBuff);  // Send Sale transaction to PayBox log receiver

}

// Utility to extract header value from headers
String getHeaderValue(String header, String headerName) {
  return header.substring(strlen(headerName.c_str()));
}

// OTA Logic
void execOTA() {

  execOTA_retcode = 0;

  Serial.println("Connecting to: " + String(ota_host));
  // Connect to S3
  if (ota_client.connect(ota_host.c_str(), ota_port)) {
    // Connection Succeed.
    // Fecthing the bin
    Serial.println("Fetching Bin: " + String(ota_bin));

    // Get the contents of the bin file
    ota_client.print(String("GET ") + ota_bin + " HTTP/1.1\r\n" +
                 "Host: " + ota_host + "\r\n" +
                 "Cache-Control: no-cache\r\n" +
                 "Connection: close\r\n\r\n");

    // Check what is being sent
    //    Serial.print(String("GET ") + ota_bin + " HTTP/1.1\r\n" +
    //                 "Host: " + ota_host + "\r\n" +
    //                 "Cache-Control: no-cache\r\n" +
    //                 "Connection: close\r\n\r\n");

    unsigned long timeout = millis();
    while (ota_client.available() == 0) {
      if (millis() - timeout > 5000) {
        ota_client.stop();
        execOTA_retcode = 401;
        sprintf(execOTA_retstr, "OTAerror[%d]: http_timeout", execOTA_retcode);
        Serial.println(execOTA_retstr);
        return;
      }
    }
    // Once the response is available,
    // check stuff

    /*
       Response Structure
        HTTP/1.1 200 OK
        x-amz-id-2: NVKxnU1aIQMmpGKhSwpCBh8y2JPbak18QLIfE+OiUDOos+7UftZKjtCFqrwsGOZRN5Zee0jpTd0=
        x-amz-request-id: 2D56B47560B764EC
        Date: Wed, 14 Jun 2017 03:33:59 GMT
        Last-Modified: Fri, 02 Jun 2017 14:50:11 GMT
        ETag: "d2afebbaaebc38cd669ce36727152af9"
        Accept-Ranges: bytes
        Content-Type: application/octet-stream
        Content-Length: 357280
        Server: AmazonS3

        {{BIN FILE CONTENTS}}

    */
    while (ota_client.available()) {
      // read line till /n
      String line = ota_client.readStringUntil('\n');
      // remove space, to check if the line is end of headers
      line.trim();

      // if the the line is empty,
      // this is end of headers
      // break the while and feed the
      // remaining `client` to the
      // Update.writeStream();
      if (!line.length()) {
        //headers ended
        break; // and get the OTA started
      }

      // Check if the HTTP Response is 200
      // else break and Exit Update
      if (line.startsWith("HTTP/1.1")) {
        if (line.indexOf("200") < 0) {
          Serial.println("Got a non 200 status code from server. Exiting OTA Update.");
          break;
        }
      }

      // extract headers here
      // Start with content length
      if (line.startsWith("Content-Length: ")) {
        contentLength = atol((getHeaderValue(line, "Content-Length: ")).c_str());
        Serial.println("Got " + String(contentLength) + " bytes from server");
      }

      // Next, the content type
      if (line.startsWith("Content-Type: ")) {
        String contentType = getHeaderValue(line, "Content-Type: ");
        Serial.println("Got " + contentType + " payload.");
        if (contentType == "application/octet-stream") {
          isValidContentType = true;
        }
      }
    }
  } else {
    // Connect to S3 failed
    // May be try?
    // Probably a choppy network?
    Serial.println("Connection to " + String(ota_host) + " failed. Please check your setup");
    // retry??
    // execOTA();
  }

  // Check what is the contentLength and if content type is `application/octet-stream`
  Serial.println("contentLength : " + String(contentLength) + ", isValidContentType : " + String(isValidContentType));

  // check contentLength and content type
  if (contentLength && isValidContentType) {
    // Check if there is enough to OTA Update
    bool canBegin = Update.begin(contentLength);

    // If yes, begin
    if (canBegin) {
      Serial.println("Begin OTA. This may take 2 - 5 mins to complete. Things might be quite for a while.. Patience!");
      // No activity would appear on the Serial monitor
      // So be patient. This may take 2 - 5mins to complete
      size_t written = Update.writeStream(ota_client);

      if (written == contentLength) {
        Serial.println("Written : " + String(written) + " successfully");
      } else {
        Serial.println("Written only : " + String(written) + "/" + String(contentLength) + ". Retry?" );
        // retry??
        // execOTA();
      }

      if (Update.end()) {
        Serial.println("OTA done!");
        if (Update.isFinished()) {
          //Serial.println("Update successfully completed. Rebooting.");
          Serial.println("Update successfully completed");
          execOTA_retcode = 1;
          sprintf(execOTA_retstr, "success");
          reboot_after_ota = true;
          //ESP.restart();
        } else {
          execOTA_retcode = 402;
          sprintf(execOTA_retstr, "OTAerror[%d]: Update not finished? Something went wrong!");
          Serial.println(execOTA_retstr);
        }
      } else {
        execOTA_retcode = 403;
        sprintf(execOTA_retstr, "OTAerror[%d]: error_occured_%s", execOTA_retcode, String(Update.getError()));
        Serial.println(execOTA_retstr);
      }
    } else {
      // not enough space to begin OTA
      // Understand the partitions and
      // space availability
      //ota_client.flush();
      execOTA_retcode = 404;
      sprintf(execOTA_retstr, "OTAerror[%d]: Not enough space to begin OTA", execOTA_retcode);
      Serial.println(execOTA_retstr);
    }
  } else {
    //ota_client.flush();
    execOTA_retcode = 405;
    sprintf(execOTA_retstr, "OTAerror[%d]: There was no content in the response", execOTA_retcode);
    Serial.println(execOTA_retstr);
  }
}

void HTTP_OTA_UpdateCheck() {

  switch (HTTP_OTA_UpdateCheck_State) {

    case 0: // IDLE
      // receive command to update firmware
      if (HTTP_OTA_run) {
        HTTP_OTA_UpdateCheck_State = 1;
      }
      break;

    case 1: // Execute OTA update
      // Execute OTA update
      execOTA();
      sprintf(sBuff, ">>> execOTA() return code = \"%d\", note = \"%s\" ", execOTA_retcode, execOTA_retstr);
      Serial.print(sBuff);
      if(ota_client.available()) {
        Serial.println(", do disconnecting from OTA sever <<<");
        ota_client.stop();
      } else {
        Serial.println(", already disconnected from OTA sever");
      }
      
      if(reboot_after_ota) {
        reboot_after_ota = false;
        HTTP_OTA_UpdateCheck_State = 3;
        sprintf(otaStr, "F1");
        writeRomString(OTA_POS, 32, otaStr);
        sprintf(otaStr, "XX");
        readRomString(OTA_POS,    32,  otaStr);
        sprintf(sBuff, "Rebooting after OTA updated... [%s]", otaStr);
        Serial.println(sBuff);
        ESP.restart();
        
      } else {
        HTTP_OTA_UpdateCheck_State = 2;      
      }
      break;          

    case 2: // OTA error : sending mqtt error

      NTPformattedDate = timeClient.getFormattedDate();
      NTPformattedDate.toCharArray(TmpCharArray, sizeof(TmpCharArray));

      sprintf(sBuff, "{ \"id\" : \"device/%s/%d\" , \"event\":\"error\", \n \"data\": {\n\"err_code\": %d, \n\"note\": \"%s\", \n\"timestamp\": \"%s\" } }", nodeIdStr, CmdDevicePort, execOTA_retcode, execOTA_retstr, TmpCharArray);
      publishMqttTopic("device/event", sBuff);
      HTTP_OTA_UpdateCheck_State = 3;      
      break;

    default: // Done
      HTTP_OTA_UpdateCheck_State = 0;
      HTTP_OTA_run = false;
  }
}      
